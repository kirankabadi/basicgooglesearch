exports.config={

    framework:'jasmine', //By default it is jasmine. We have to mention this only AMocha or Cucumber
    seleniumAddress:'http://localhost:4444/wd/hub', //default localhost for protractor to start selenium server

    specs:['searchSpec.js'],
    capabilities:{
        browserName: 'chrome',
    },

};
