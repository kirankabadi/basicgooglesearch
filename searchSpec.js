describe('Google search', function () {

    browser.ignoreSynchronization = true;

    var searchText = 'Hubli'
    it('launch browser in this step', function () {

        browser.get("http://www.google.com");
        expect(browser.getTitle()).toEqual('Google');

        element(by.className('gLFyf gsfi')).sendKeys(searchText).then(function () {
            browser.sleep('5000');
        });

        element(by.xpath('//input[@value="Google Search"]')).click().then(function () {
            browser.sleep('9000');
        })
    });

    it('validate if search results are found', function () {


        //Below code validates the main search title, to validate the search result.
        var searchHeaderText = element(by.xpath('//div[@data-attrid="title"]'));
        expect(searchHeaderText.getText()).toContain(searchText);


        //Below text validate the text from the search results links.
        //Below code returns error for validation.
        // element.all(by.xpath('//div[@class="bkWMgd"]//h3[@class="LC20lb"]')).each(function(element, index) {
        //
        //     element.getText().then(function (text) {
        //         console.log(index, text);
        //         expect(text).toContain(searchText);
        //     });
        // });

    })
});
